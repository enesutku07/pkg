install:
	mkdir $(PREFIX)/usr/lib/pkg || true
	cp -prf ./* $(PREFIX)/usr/lib/pkg/
	chmod +x $(PREFIX)/usr/lib/pkg/pkg
	rm -rf $(PREFIX)/bin/pkg
	ln -s /usr/lib/pkg/pkg $(PREFIX)/bin/pkg || true
	mkdir -p $(PREFIX)/var/packages/ || true
	cp -prf test-index.txt $(PREFIX)/var/packages/index.txt
